package edu.galaxytrainig.selenium;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.edge.EdgeDriver;

public class HelloEdge {

	static String path = "";
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		path = System.getProperty("user.dir");
		
		System.out.println(path);
		
		System.setProperty("webdriver.edge.driver", path + "/src/test/resources/driver/msedgedriver.exe");

		WebDriver driver = new EdgeDriver();
		
		driver.get("https://mvnrepository.com/");
		Thread.sleep(5000);// se debera agregar exception
		//cierra ventana mas no puereto (hilo)
		//=====>driver.close();
		//cierra ventana y cierra hilo de ejecucion (puerto)
		driver.quit();
	}

}
