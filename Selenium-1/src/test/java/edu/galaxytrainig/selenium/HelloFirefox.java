package edu.galaxytrainig.selenium;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.firefox.FirefoxDriver;

public class HelloFirefox {

	static String path = "";
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		path = System.getProperty("user.dir");
		
		System.out.println(path);
		System.setProperty("webdriver.gecko.driver", path + "/src/test/resources/driver/geckodriver.exe");

		WebDriver driver = new FirefoxDriver();
		
		driver.manage().window().maximize();
		driver.get("https://mvnrepository.com/");
		Thread.sleep(2000);// se debera agregar exception
		//cierra ventana mas no puereto (hilo)
		//=====>driver.close();
		//cierra ventana y cierra hilo de ejecucion (puerto)
		driver.quit();
	}

}
