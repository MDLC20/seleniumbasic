package com.galaxy.project.extend;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.galaxy.project.login.page.loginPageObject;

public class ExtendReport {
	
	private static WebDriver driver;
	private static String url = "http://automationpractice.com/index.php";
	
	ExtentHtmlReporter report = new ExtentHtmlReporter("./reports/login.html");
	ExtentReports extent = new ExtentReports();
	
	@BeforeMethod
	public void setup() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get(url);
	}
	
	@Test(priority = 1, description ="Logeo con exito")
	public  void testLoginPageObject() throws InterruptedException {
		extent.attachReporter(report);
		ExtentTest logger = extent.createTest("Logeo exitoso");
		
		loginPageObject lpo = new loginPageObject(driver);
		lpo.goLoginPage();
		lpo.completedLoginPageObject("micorreoprueba987@gmail.com", "correopruebas");
		Thread.sleep(1000);
		lpo.clickedLoginPage();
		Thread.sleep(2000);
		lpo.tearDown();
		
		logger.log(Status.PASS, "verify title");
		extent.flush();
	}
	@Test(priority = 2, description ="Logeo sin exito")
	public  void testLoginPageObjectFaild() throws InterruptedException {
		extent.attachReporter(report);
		ExtentTest logger1 = extent.createTest("error en logeo usuario");
		
		loginPageObject lpo = new loginPageObject(driver);
		lpo.goLoginPage();
		lpo.completedLoginPageObject("correoprueba987@gmail.com", "correopruebas");
		Thread.sleep(1000);
		lpo.clickedLoginPage();
		Thread.sleep(2000);	
		lpo.tearDown();
		logger1.log(Status.FAIL, "error en logeo usuario");
		extent.flush();
	}
	@Test(priority = 3, description ="Logeo sin exito")
	public  void testLoginPageObjectFaildSecondCase() throws InterruptedException {
		extent.attachReporter(report);
		ExtentTest logger2 = extent.createTest("error en logeo password");
		
		loginPageObject lpo = new loginPageObject(driver);
		lpo.goLoginPage();
		lpo.completedLoginPageObject("micorreoprueba987@gmail.com", "micorreopruebas");
		Thread.sleep(1000);
		lpo.clickedLoginPage();
		Thread.sleep(2000);	
		lpo.tearDown();
		logger2.log(Status.FAIL, "error en logeo password");
		extent.flush();
	}
	
}
