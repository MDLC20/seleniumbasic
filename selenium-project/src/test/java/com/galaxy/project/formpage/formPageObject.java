package com.galaxy.project.formpage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class formPageObject {

	protected static WebDriver driver;
	
	private By frist_name = By.name("firstname");
	private By last_name = By.name("lastname");	
	private By dates = By.id("datepicker");
	
	private By cbox_sexo = By.cssSelector(".radio:nth-child(4)");
	private By cbox_experiences = By.id("exp-1");	
	private By cbox_professions = By.id("profession-0");	
	private By cbox_tools = By.id("tool-2");
	
	private By cbox_inputs = By.xpath("//option[. = 'Africa']");	
	private By cbox_comands = By.xpath("//option[. = 'WebElement Commands']");	
	
	//private By photos = By.id("photo");	
	private By downloads  = By.linkText("Click here to Download File");
	
	public formPageObject(WebDriver driver) {
		this.driver = driver;
	}
	
	public void completedFormPageObject(String nombre, String apellido, String cumple) {
		
		driver.findElement(frist_name).sendKeys(nombre);
		driver.findElement(last_name).sendKeys(apellido);
		driver.findElement(dates).sendKeys(cumple);
		}
		
	public void completedCheckBoxPageObject() {
		driver.findElement(cbox_sexo).click();
		driver.findElement(cbox_experiences).click();
	    driver.findElement(cbox_professions).click();
	    driver.findElement(cbox_tools).click();
	}	
	
	public void selectedComboxPageObject() {
		driver.findElement(cbox_inputs).click();
		driver.findElement(cbox_comands).click();		
	}
	
	public void clickedTextPageObject() {		
		//driver.findElement(photos).click();
		driver.findElement(downloads).click();
	}
	public void tearDown() {
		driver.quit();
	}
	
}
