package com.galaxy.project.login.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class loginPageObject {

	protected static WebDriver driver;
	
	
	private By registrar  = By.linkText("Sign in");
	
	private By user = By.id("email");
	private By pass = By.id("passwd");
	
	private By logear = By.cssSelector("#SubmitLogin > span");
	
	public loginPageObject(WebDriver driver) {
		this.driver = driver;
	}
	
	public void goLoginPage() {
		driver.findElement(registrar).click();
	}
	public void completedLoginPageObject(String usuario, String password) {	
		driver.findElement(user).sendKeys(usuario);
		driver.findElement(pass).sendKeys(password);
		}
	public void clickedLoginPage() {
		driver.findElement(logear).click();
	}
	public void tearDown() {
		driver.quit();
	}
	
}
