package com.galaxy.project.login;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.firefox.FirefoxDriver;

import org.testng.annotations.Test;

import com.galaxy.project.login.page.loginPageObject;

public class loginTest {


	 private static WebDriver driver;
		
	 private static String url = "http://automationpractice.com/index.php";
	
	

	public void setup() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get(url);
	}

	@Test(priority = 1, description ="Logeo con exito")
	public  void testLoginPageObject() throws InterruptedException {
		loginPageObject lpo = new loginPageObject(driver);
		driver.manage().window().maximize();
		driver.get(url);
		lpo.goLoginPage();
		lpo.completedLoginPageObject("micorreoprueba987@gmail.com", "correopruebas");
		Thread.sleep(1000);
		lpo.clickedLoginPage();
		Thread.sleep(10000);	
		lpo.tearDown();
	}
	@Test(priority = 2, description ="Logeo sin exito")
	public  void testLoginPageObjectFaild() throws InterruptedException {
		loginPageObject lpo = new loginPageObject(driver);
		lpo.goLoginPage();
		lpo.completedLoginPageObject("correoprueba987@gmail.com", "correopruebas");
		Thread.sleep(1000);
		lpo.clickedLoginPage();
		Thread.sleep(10000);	
		lpo.tearDown();
	}

}
