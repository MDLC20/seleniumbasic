package com.galaxy.project.tests;

import org.junit.After;
import org.junit.Before;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.galaxy.project.formpage.formPageObject;

public class formTests {

	 static private WebDriver driver;
	
	 static private String url = "https://www.techlistic.com/p/selenium-practice-form.html";
/*	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		completedFormPageObject();
	}*/
   @BeforeMethod
	public  void setUp() {
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get(url);
	}
	@Test
	public  void completedFormPageObject() throws InterruptedException {
		formPageObject fpo = new formPageObject(driver);
		
		fpo.completedFormPageObject("M", "DLC", "0000-00-00");
		fpo.completedCheckBoxPageObject();
		Thread.sleep(1000);
		fpo.selectedComboxPageObject();
		Thread.sleep(1000);
		fpo.clickedTextPageObject();
		Thread.sleep(1000);
//		fpo.tearDown();
	}
	@After
	public void exitTest() {
	    driver.quit();	
	}
	
}
